#-*- encoding: utf-8 -*-
import os
import sys
import BeautifulSoup
import urllib2

from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sf.settings")
application = get_wsgi_application()
from django.conf import settings
from films.models import *



good_words = [
    u"хороший",
    u"хорошее",
    u"приятн",
    u"прикольн",
    u"позитивный",
    u"спасибо",
    u"отличн",
    u"супер",
    u"шикарн",
    u"фильм хорош",
    u"офигенный",
    u"благодарствую",
    u"советую",
    u"охрененный",
    u"классный",
    u"понравился",
    u"неожиданный сюжет",
    u"атмосферный",
]
bad_words = [
    u"безобразный",
    u"тупой",
    u"тупейший",    
    u"ни о чём",
    u"ни о чем",
    u"не о чем",    
    u"скучный",
    u"скучно",
    u"унылый",
    u"нудный",    
    u"гауно",
    u"гадкий",
    u"зря потраченного времени.",
    u"затянуто",
    u"лажа",
    u"бред",
    u"хрень",
    u"чушь",
    u"шляпа",
]


def downloadPage(link):
    #tm = 1
    #tm = random.randint(5,10)
    #print "sleep=",tm
    #time.sleep(tm)
    print "downloading=",link
    req = urllib2.Request(link, None, {
        'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/43.0',
        'Host':'rutracker.org',
        })
    response = urllib2.urlopen(req)
    """
    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError, err:
        if err.code == 404: return ""
        print "ERROR",link
        raise x
    """
    #print "reading"
    return response.read()

def downloadComment(uid,data):
    if "windows-1251" in data.lower(): data = data.decode('cp1251')
    #print data
    fm = Film.objects.get(uid=uid)
    soup = BeautifulSoup.BeautifulSoup(data)
    total_goods = 0
    total_bads  = 0
    for d in soup.findAll("div",{"class":"post_body"}):
        if "span" in d.text: continue
        if "bitrate" in d.text: continue
        if "Duration" in d.text: continue
        if "VirtualDubMod" in d.text: continue
        text = d.text.lower() 
        #print len(text)
        if len(text)>200: continue
        if Comment.objects.filter(film=fm,text=text).count()>0: continue
        Comment(film=fm,text=text).save()
    
    
def recalcComments(fm):
    total_goods = 0
    total_bads  = 0
    for d in Comment.objects.filter(film=fm).exclude(text=None):
        if "span" in d.text: continue
        if "bitrate" in d.text: continue
        if "Duration" in d.text: continue
        if "VirtualDubMod" in d.text: continue
        text = d.text.lower() 
        goods = 0
        bads  = 0
        for w in good_words:
            if w.lower() in text:
                goods+=1
        for w in bad_words:
            if w.lower() in text:
                bads+=1
        
        total_goods += goods
        total_bads  += bads*2
    #print total_goods,total_bads
    fm.cnt_good_comment = total_goods
    fm.cnt_bad_comment = total_bads
    fm.save()


def analizeComments():
    """
    for c in Comment.objects.all().exclude(text=None):
        x = "".join([d for d in c.text if (d.isalpha() or d==" ")])
        for w in x.split(" "):
            if len(w)<4: continue
            Word.objects.get_or_create(word=w)
    
    print "wcount=",Word.objects.all().count()
    """
    train_data = []
    need_data  = []
    for c in Comment.objects.all().exclude(text=None):
        x = "".join([d for d in c.text if (d.isalpha() or d==" ")])
        data = []
        for w in x.split(" "):
            if len(w)<4: continue
            w,cr = Word.objects.get_or_create(word=w)
            data.append(str(w.id))
        c.ctext = " ".join(data)
        c.save()
        if c.xrate>0:
            train_data.append("%s:%d"%(c.ctext,c.xrate-1))
            
        need_data.append(c.ctext)
    
    f = file("train.txt","w")
    for d in train_data:
        f.write(d+"\n")
    f.close()
    
    f = file("need.txt","w")
    for d in need_data:
        f.write(d+"\n")
    f.close()
    

def readResults():
    if not os.path.exists("result.txt"): return
    f = file("result.txt","r")
    for d in f.read().split("\n"):
        if d.strip()=="": continue
        w = d.split(":")[0]
        r = int(d.split(":")[1])+1
        #Comment.objects.filter(ctext=w).count()
        Comment.objects.filter(ctext=w).update(nsrate=r)
        
    f.close()
        
        




def downloadComments():
    for d in Film.objects.all().exclude(link=None):
        if Comment.objects.filter(film=d,is_rm=False).count()>30: continue
        if '://rutracker.org/forum/' not in d.link: 
            d.link = 'http://rutracker.org/forum/'+d.link
            d.save()
            
        plink = [d.link]
        fpage = downloadPage(d.link)
        downloadComment(d.uid,fpage)
        soup = BeautifulSoup.BeautifulSoup(fpage)
        for x in soup.findAll("a",{"class":"pg"}):
            href = 'http://rutracker.org/forum/'+x['href']
            if href in plink: continue
            npage = downloadPage(href)
            downloadComment(d.uid,npage)
            plink.append(href)
        
            
if __name__=="__main__":
    if len(sys.argv)<2:
        downloadComments()
        #analizeComments()   
        #readResults()
        exit()
    
    uid = sys.argv[1]
    print "uid=",uid
    
    f = Film.objects.get(uid=uid)
    downloadComment(uid,downloadPage(f.link))
