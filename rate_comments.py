#-*- encoding: utf-8 -*-

import json
import os
import datetime
import time
import django
import sys
import urllib2

from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.models import model_from_json

import tensorflow as tf
tf.python.control_flow_ops = tf

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sf.settings")
django.setup()

from django.conf import settings
from django.db import connection
from films.models import *
                
def analizeWords():    
    WORDS = {}
    for w in Word.objects.all():
        WORDS[w.word]=str(w.id)
    
    
    for c in Comment.objects.filter(ctext=None,is_rm=False).exclude(text=None):
        x = "".join([d for d in c.text.lower() if (d.isalpha() or d==" ")])
        data = []
        for w in x.split(" "):
            if len(w)<4: continue
            if not w in WORDS.keys():                
                wd,cr = Word.objects.get_or_create(word=w)
                WORDS[w]=str(wd.id)
            
            data.append(WORDS[w])
        c.ctext = " ".join(data)
        c.save()
    
    del WORDS

def prepareModel(maxlen):    
    MAX_FEATURES = Word.objects.all().count()+1
    model = Sequential()
    model.add(Embedding(MAX_FEATURES, 128, input_length=maxlen))
    model.add(LSTM(64, return_sequences=True))
    model.add(LSTM(64))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    print "compiling model"
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=["accuracy"])
                  
    return model
        

def makeDesition():
    batch_size = 32
    maxlen = 0

    X_train = []
    Y_train = []
    need = []

    # обучать
    for d in Comment.objects.filter(xrate=2,is_rm=False).exclude(ctext=None): # хороший комментарий
        X_train.append([int(x) for x in d.ctext.split(" ") if x!=''])
        Y_train.append([1])
        maxlen  += 1

    for d in Comment.objects.filter(xrate=1,is_rm=False).exclude(ctext=None): # плохой комментарий
        X_train.append([int(x) for x in d.ctext.split(" ") if x!=''])
        Y_train.append([0])
        maxlen  += 1
    
    # анализировать
    for d in Comment.objects.filter(xrate=0,is_rm=False).exclude(ctext=None):
        need.append([int(x) for x in d.ctext.split(" ") if x!=''])

    X_train = sequence.pad_sequences(X_train, maxlen=maxlen)

    model = prepareModel(maxlen)
    print "fitting"

    model.fit(
        X_train, Y_train, 
        batch_size=batch_size, 
        nb_epoch=20
    )
    print "proba"
    x = sequence.pad_sequences(need, maxlen=maxlen)
    result = model.predict_proba(x)
    
    for d in need:
        x = sequence.pad_sequences([d], maxlen=maxlen)
        result = model.predict_proba(x)
        #print "result=",result[0][0]
        r = 0 if result[0][0]<0.5 else 1
        if r==0:
            print "BAD COMMENT",result
            print Comment.objects.filter(ctext=" ".join([str(x) for x in d]))[0].film.uid,Comment.objects.filter(ctext=" ".join([str(x) for x in d]))[0].text
        Comment.objects.filter(ctext=" ".join([str(x) for x in d])).update(nsrate=r+1)

def checkForRemove():
    batch_size = 32
    maxlen = 0

    X_train = []
    Y_train = []
    need = []

    
    for d in Comment.objects.filter(is_rm=True).exclude(ctext=None):
        X_train.append([int(x) for x in d.ctext.split(" ") if x!=''])
        Y_train.append([1])
        maxlen  += 1
    
    for d in Comment.objects.filter(xrate__gt=0,is_rm=False):
        X_train.append([int(x) for x in d.ctext.split(" ") if x!=''])
        Y_train.append([0])
        maxlen  += 1
    """
    for d in Comment.objects.filter(is_rm=False,film__uid="mgla").exclude(ctext=None):
        need.append([int(x) for x in d.ctext.split(" ") if x!=''])
    """
    for d in Comment.objects.filter(is_rm=False).exclude(ctext=None):
        need.append([int(x) for x in d.ctext.split(" ") if x!=''])
    
    X_train = sequence.pad_sequences(X_train, maxlen=maxlen)

    model = prepareModel(maxlen)
    print "fitting"

    model.fit(
        X_train, Y_train, 
        batch_size=batch_size, 
        nb_epoch=20
    )
    print "proba"
    x = sequence.pad_sequences(need, maxlen=maxlen)
    result = model.predict_proba(x)
    
    for d in need:
        x = sequence.pad_sequences([d], maxlen=maxlen)
        result = model.predict_proba(x)
        #print "result=",x,result
        r = 0 if result[0][0]<0.5 else 1
        if r==1:
            print "FOR REMOVE",result
            print Comment.objects.filter(ctext=" ".join([str(x) for x in d]))[0].text
            #Comment.objects.filter(ctext=" ".join([str(x) for x in d])).update(rm=True)    

if __name__=="__main__":
    tm = time.time()
    print "analizing words"
    analizeWords()    
    print "checking for remove"
    checkForRemove()
    print "rate comments"
    #makeDesition()
    print "tm=",time.time()-tm
    
    
    

