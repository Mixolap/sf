#-*- encoding: utf-8 -*-
#from bs4 import BeautifulSoup
from BeautifulSoup import BeautifulSoup
import django,os,sys,datetime,subprocess,random,time,urllib2,re,urllib

from PIL import Image
from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sf.settings")
application = get_wsgi_application()
from films.models import *

CHECKED_FILMS = []

def downloadPage(link):
    tm = 1
    tm = random.randint(5,10)
    print "sleep=",tm
    time.sleep(tm)
    print "downloading=",link
    req = urllib2.Request(link, None,     headers = { 
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language':'ru,en-us;q=0.7,en;q=0.3',
    'Cache-Control':'max-age=0',
    'Connection':'keep-alive',
    'Cookie':'user_country=ru; my_perpages=a%3A0%3A%7B%7D; __utma=168025531.1625787791.1343102616.1355074080.1355076436.37; __utmz=168025531.1355076436.37.37.utmcsr=ftpface.ru|utmccn=(referral)|utmcmd=referral|utmcct=/file/safe_2012_dub_ustransfer_bdrip_xvid_ac3_avi.html; last_visit=2012-12-09+22%3A18%3A30; mobile=no; __utmb=168025531.5.10.1355076436; PHPSESSID=8fe12aa58fa12a34754bad22355a5c85; __utmc=168025531',
    'Host':'www.kinopoisk.ru',
    'Referer':'http://ftpface.ru/file/safe_2012.html',
    'User-Agent':'Mozilla/5.0 (X11; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0',
    }  )
    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError, err:
        if err.code == 404: return ""
        print "ERROR",link
        raise x
    return response.read()


def getId(text):
    #print "data=",text
    m = re.search(r'/film/(\d+)/',text)
    if m==None: return
    return int(m.group(1))    

def infoData(txt):
    if txt==None: return 
    #print "txt=",txt
    data = []        
    #soup = BeautifulSoup(txt)
    object = re.compile('<a .*>(.*)</a>')    
    for f in txt.findAll('a'):
        #print "f=",f.getText()        
        x = object.search(str(f)).group(1)
        #print x
        if x=="...": continue
        data.append(x)
    return data
    
def saveImg(id,data,path="/opt/sf/static/imgs/"):
    fpath = path+"%d.jpg"%id
    print "saving",fpath
    f = file(fpath,"w")
    f.write(data)
    f.close()

def downloadImg(url,img_href):
    headers = { 
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language':'ru,en-us;q=0.7,en;q=0.3',
    'Cache-Control':'max-age=0',
    'Connection':'keep-alive',
    'Cookie':'user_country=ru; my_perpages=a%3A0%3A%7B%7D; __utma=168025531.1625787791.1343102616.1355074080.1355076436.37; __utmz=168025531.1355076436.37.37.utmcsr=ftpface.ru|utmccn=(referral)|utmcmd=referral|utmcct=/file/safe_2012_dub_ustransfer_bdrip_xvid_ac3_avi.html; last_visit=2012-12-09+22%3A18%3A30; mobile=no; __utmb=168025531.5.10.1355076436; PHPSESSID=8fe12aa58fa12a34754bad22355a5c85; __utmc=168025531',
    'Host':'www.kinopoisk.ru',
    'Referer':'http://ftpface.ru/file/safe_2012.html',
    'User-Agent':'Mozilla/5.0 (X11; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0',
    }    
    headers['Referer'] = url
    req = urllib2.Request(img_href, None, headers)
    return urllib2.urlopen(req).read()          

def load_info_from_kinopoisk(data,url):
    soup = BeautifulSoup(data) 
    img_href = soup.find("img",itemprop="image")
    if img_href==None: return 
    img_href = img_href["src"]    
    fi = {}
    fi["name"] = soup.findAll("h1",itemprop="name")[0].getText().replace("&nbsp;"," ")
    if u"сериал" in fi["name"]: return 
    #if u"(видео)" in fi["name"]: return 
    fi["name_orig"] = soup.find("span",{"itemprop":"alternativeHeadline"}).getText()
    try:
        fi["description"] = soup.findAll("div",itemprop="description")[0].getText()
    except: 
        fi["description"] = ""
    try:
        fi["kp_raiting"] = float(soup.find("meta",itemprop="ratingValue")['content'])
    except: 
        fi["kp_raiting"]=0
    if fi["kp_raiting"]==0: 
        print "SKIPPING raiting 0",fi["name"]
        return
    data = []
    for t in soup.findAll("li",itemprop="actors"):
        if len(infoData(t))==0: continue
        data.append(infoData(t)[0])
        if len(data)>=5: break
    fi["actors"] = data
    
    # поиск примьера Россия
    xsoup = soup
    soup = xsoup.find("table",{"class":"info"})#BeautifulSoup(html)
    if soup==None: soup = xsoup.find("table",{"class":"info "})#BeautifulSoup(html)
    if soup==None: soup = xsoup.find("table",{"class":"info nooriginalPoster"})
    tags = soup.findAll('td')

    data = []
    for t in soup.findAll("span",itemprop="genre"):
        for d in t.findAll("a"):
            #if d.getText()==u"мультфильм": return None
            if d.getText()==u"документальный": return None
            data.append(d.getText())
    
    fi["genre"] = data
    ydata = infoData(tags[1])
    if len(ydata)==0: return None
    fi["year"]     = ydata[0]
    fi["country"]  = infoData(tags[3])
    fi["producer"] = infoData(tags[7])
    if int(ydata[0])<1980: return None
    # поиск премьера Россия
    e = soup.find("td",{"id":"div_rus_prem_td2"})
    if e!=None:
        try:
            fi["dt_out"] = datetime.datetime.strptime(e.find("div",{"class":"prem_ical"})["data-date-premier-start-link"],"%Y%m%d")
        except:
            fi["dt_out"] = datetime.datetime(int(fi["year"]),1,1)
    else:
        fi["dt_out"] = datetime.datetime(int(fi["year"]),1,1)
    fi["img_data"] = downloadImg(url,img_href)
    return fi

def findSimilar(id):
    url = "http://www.kinopoisk.ru/film/%d/like/"%id
    data     = downloadPage(url)
    soup = BeautifulSoup(data) 
    data = []
    if soup.find("table",{"class":"ten_items"})==None: return data
    for d in soup.find("table",{"class":"ten_items"}).findAll("a",{"class":"all"}):
        data.append(getId(d["href"]))
    return data
    
    

def downloadFilm(id,fs=True):
    if id==None: return None
    if int(id) in CHECKED_FILMS: return None
    CHECKED_FILMS.append(id)
    if Film.objects.filter(kp_id=id).count()>0:         
        f = Film.objects.filter(kp_id=id)[0]
        if not fs: return f
        if SimilarFilm.objects.filter(film1=f).count()>3: return f
        for d in findSimilar(id):
            sf = downloadFilm(d,False)
            if sf==None: continue
            SimilarFilm.objects.get_or_create(film1=f,film2=sf)        
        return f
    url      = "http://www.kinopoisk.ru/film/%d/"%id
    data     = downloadPage(url)
    info     = load_info_from_kinopoisk(data,url)
    if info==None: return
    print "downloaded",info["name"],info["name_orig"],info["kp_raiting"]
    f = Film(kp_id=id)
    f.name          = info["name"]
    f.name_orig     = info["name_orig"]
    f.year          = info["year"]
    if int(f.year)<1980: return 
    f.dt_out        = info["dt_out"]
    f.description   = info["description"]
    f.kp_raiting    = info["kp_raiting"]
    f.save()
    print "saved",f.name,f.id
    saveImg(f.id,info["img_data"])
    
    for d in info['actors']:
        v,cr = Actor.objects.get_or_create(name=d.strip())
        FilmActor.objects.get_or_create(film=f,actor=v)
        print "actor=",d,cr,v.id
        
    for d in info['producer']:        
        v,cr = Producer.objects.get_or_create(name=d.strip())
        FilmProducer.objects.get_or_create(film=f,producer=v)        
        print "producer=",d,cr,v.id
        
    for d in info['genre']:        
        v,cr = Genre.objects.get_or_create(name=d.strip())
        FilmGenre.objects.get_or_create(film=f,genre=v)        
        print "genre=",d,cr,v.id
    for d in info['country']:
        v,cr = Country.objects.get_or_create(name=d.strip())
        FilmCountry.objects.get_or_create(film=f,country=v)        
        print "country=",d,cr,v.id
        
    for d in findSimilar(id):
        sf = downloadFilm(d)
        if sf==None: continue
        SimilarFilm.objects.get_or_create(film1=f,film2=sf)
        #print "similar=",d
    
    return f
    

def downloadCatalog(url):
    data = downloadPage(url)
    soup  = BeautifulSoup(data)
    
    if soup.find("div",{"class":"stat"})==None:
        for d in soup.find("div",{"class":"name"}).findAll("a"):
            fid = getId(d["href"])
            downloadFilm(fid)        
    else:
        for d in soup.find("div",{"class":"stat"}).findAll("div"):
            if len(d.findAll("a"))<2: continue
            fhref = d.findAll("a")[1]['href']
            fid = getId(fhref)
            if fid==None:
                print "no id",fhref
                continue
            downloadFilm(fid)        
        #break

"""
for f in Film.objects.filter(year__gte=2000).order_by("-kp_raiting"):
    sf1 = SimilarFilm.objects.filter(film1=f).count()
    sf2 = SimilarFilm.objects.filter(film2=f).count()
    if sf1+sf2<5:
        for d in findSimilar(f.id):
            sf = downloadFilm(d)
            if sf==None: continue
            SimilarFilm.objects.get_or_create(film1=f,film2=sf)
"""

#Film.objects.all().delete()

def downloadPopular():
    for d in range(1,5):
        url = "http://www.kinopoisk.ru/popular/page/%d/"%d
        downloadCatalog(url)
        #downloadFilm(77454)
        print url

def downloadGenre():
    for d in range(1,47):
        url = "http://www.kinopoisk.ru/lists/ord/name/m_act%%5Bgenre%%5D/20/m_act%%5Ball%%5D/ok/page/%d/"%d
        #print url
        downloadCatalog(url)
def nameId(text):
    m = re.search(r'/name/(\d+)/',text)
    if m==None: return
    return int(m.group(1))    
    
def downloadActors():
    for d in Actor.objects.filter(kp_id=None).order_by("?"):
        url = "http://www.kinopoisk.ru/index.php?first=yes&what=&"+urllib.urlencode({"kp_query":d.name.encode('utf-8')})
        data = downloadPage(url)
        bs = BeautifulSoup(data)
        if bs.find('link',{'rel':'canonical'})==None:
            f = file("download.html","wb")
            f.write(data)
            f.close()
        e_link = bs.find('link',{'rel':'canonical'})
        #print e_link.attrMap.keys()
        if e_link==None or "href" not in e_link.attrMap.keys(): continue
        kp_id = nameId(e_link["href"])
        print d.name,"kp_id=",kp_id
        if kp_id==None: continue            
        e_img = bs.find('div',{'class':'film-img-box'}).find("img")
        print e_img
        if e_img==None or e_img.attrMap==None or "src" not in e_img.attrMap.keys(): 
            print "NO IMG continuing"
            continue
        print "saving image",d.id
        img_href = e_img["src"]
        data = downloadImg(url,img_href)
        saveImg(d.id,data,"/opt/sf/static/name_img/")
        d.kp_id=kp_id
        d.save()
        #break
    
def downloadTop():
    #for d in range(1,5):
    data = downloadPage("https://www.kinopoisk.ru/top/")
    soup = BeautifulSoup(data)
    for d in soup.findAll("a",{"class":"all"}):
        href = d["href"]
        if not "film" in href: continue
        id = getId(href)
        if id==None: continue
        print "downloading","https://www.kinopoisk.ru"+href
        downloadFilm(id)
    


if __name__=="__main__":

    if len(sys.argv)>1:
        fid = sys.argv[1]        
        downloadFilm(int(fid))
        exit()   

    #downloadGenre()
    #downloadPopular()
    #downloadActors()
    
    downloadTop()

