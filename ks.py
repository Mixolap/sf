from keras.preprocessing import sequence
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
"""
max_features = 100000
maxlen = 100 
batch_size = 32
"""





max_features = 10000
maxlen = 0
batch_size = 32


X_train = []
Y_train = []
need = []

f = file("train.txt","r")
for d in f.read().split("\n"):
    if d=="": continue
    X_train.append([int(x) for x in d.split(":")[0].split(" ") if x!=''])
    Y_train.append([int(x) for x in d.split(":")[1] if x!=''])
    maxlen  += 1
    #if len(d.split(":")[0].split(" "))>max_features: max_features = len(d.split(":")[0].split(" "))
    
f.close()

f = file("need.txt","r")
for d in f.read().split("\n"):
    need.append([int(x) for x in d.split(" ") if x!=''])

X_train = sequence.pad_sequences(X_train, maxlen=maxlen)


model = Sequential()
model.add(Embedding(max_features, 128, input_length=maxlen))
model.add(LSTM(64, return_sequences=True))
model.add(LSTM(64))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))




print "compiling"
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              #optimizer=None,
              #optimizer='fast_compile',
              class_mode="binary")
print "fitting"

model.fit(
    X_train, Y_train, 
    batch_size=batch_size, 
    nb_epoch=50,
    show_accuracy=True
)
print "proba"
x = sequence.pad_sequences(need, maxlen=maxlen)
result = model.predict_proba(x)
#print "result=",result

bad_count  = 0
good_count = 0
f = file("result.txt","w")
for d in need:
    x = sequence.pad_sequences([d], maxlen=maxlen)
    result = model.predict_proba(x)
    print "result=",result
    r = 0 if result[0][0]<0.5 else 1
    f.write("%s:%d\n"%(" ".join([str(x) for x in d]),r))
    if r==0: bad_count += 1
    else: good_count += 1
f.close()

print "good_count=",good_count,"bad_count=",bad_count

    

