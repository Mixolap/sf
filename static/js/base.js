$(function(){
	$(document).on('click','.hide_film',function(){
		var id  = $(this).attr("rel")
		var url = "/f/hide/"+id+"/"
		var target = $(this).parent().parent().parent().parent()
		if(target.find(".genre_value").length>0)
			url = "/genre/"+target.find(".genre_value").val()+"/?hide_id="+id+"&cnt="+target.find(".film_block").length
		
		$.get(url,function(data){
			 target.html(data)
		 	 $('[data-toggle="tooltip"]').tooltip({html:true,placement:'auto'});
		})
		return false;
	})
	
	$(document).on('click','.like_film',function(){
	    var _t = $(this)
		var id  = $(this).attr("rel")
		var url = "/f/like/"+id+"/"
		if(_t.attr("data-noreload")=="1") url += "?noreload=1";
		var target = $(this).parent().parent().parent().parent()
		if(target.find(".genre_value").length>0)
			url = "/genre/"+target.find(".genre_value").val()+"/?like_id="+id+"&cnt="+target.find(".film_block").length
		var target = $(this).parent().parent().parent().parent()
		$.get(url,function(data){
		    _t.hide()
		    if(_t.attr("data-noreload")=="1") return;		    
		    target.html(data)
        	$('[data-toggle="tooltip"]').tooltip({html:true,placement:'auto'});
		})
		return false;
	})

	$(document).on('click','.bookmark_film',function(){
		var _t = $(this)
		var id  = $(this).attr("rel")				
		var url = "/f/bookmark/"+id+"/"
		if(_t.attr("data-noreload")=="1") url += "?noreload=1";
		var target = $(this).parent().parent().parent().parent()
		if(target.find(".genre_value").length>0)
			url = "/genre/"+target.find(".genre_value").val()+"/?bookmark_id="+id+"&cnt="+target.find(".film_block").length
		
		$.get(url,function(data){
 		     _t.hide();
		     if(_t.attr("data-noreload")=="1") return;		     
			 target.html(data)
 		 	 $('[data-toggle="tooltip"]').tooltip({html:true,placement:'auto'});
		})
		return false;
	})	
	
	
	$(".rm_film").click(function(){	
	    $(this).parent().parent().parent().parent().next().hide()
	    $(this).parent().parent().parent().parent().hide()
	    
	    $.get($(this).attr("rel"))	
	})
	/*
	$(document).on('mouseenter','.film_img',function(){
		$(this).find(".con_block").fadeIn()
	})
	$(document).on('mouseleave','.film_img',function(){
		$(this).find(".con_block").hide()
	})*/
	
	$('[data-toggle="tooltip"]').tooltip({html:true,placement:'auto'});
});

