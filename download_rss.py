#-*- encoding: utf-8 -*-
import gc
import feedparser
import requests
import os
from bs4 import BeautifulSoup
from django.utils import timezone

from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sf.settings")
application = get_wsgi_application()
from films.models import Torrent,Film




def downloadRss(link,year):
    fd = feedparser.parse(link)    
    for d in fd.entries:
        #if GroupTopic.objects.filter(group=group,title=d.title).count()>0: continue
        #print "saving",d.title,d.link
        if not ('HDRip' in d.title or 'DVDRip' in d.title or 'BDRip' in d.title): continue
        tr,cr = Torrent.objects.get_or_create(link=d.link,defaults={'name':d.title})
        tr.filmname = tr.procName()
        tr.save()
        
    for tr in Torrent.objects.filter(film=None,is_removed=False):
        films = Film.objects.filter(name__iexact=tr.filmname,year=year)
        if films.count()!=1: continue
        f = films[0]
        if f.link!=None:
            tr.is_removed=True
            tr.save()
            continue
        f.link = tr.link
        f.dt_link = timezone.now()        
        f.save()
        tr.is_removed = True
        tr.film = f        
        tr.save()
        
def updateMagnet():
    for d in Film.objects.filter(link__isnull=False,is_downloaded=False,magnet__isnull=True).order_by("dt_link"):
        print d.link
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0',
        }
        data = requests.get(d.link,headers=headers).content
        f = file("1.html","wb")
        f.write(data)
        f.close()
        
        soup = BeautifulSoup(data)
        if soup.find("div",{"class":"attach_link"})==None: 
            #d.link = None
            #d.save()
            continue
        d.magnet = soup.find("div",{"class":"attach_link"}).find("a")['href']
        print "magnet=",d.magnet
        d.save()
        gc.collect()
        #gt = GroupTopic(group=group)
        #gt.author_id = 1
        #gt.title = d.title
        #gt.text = d.description
        #gt.link = d.link
        #gt.save()
print Film.objects.filter(dt_link__isnull=False).count()        
#Film.objects.filter(dt_link__isnull=False).update(dt_link=None,link=None)
#Torrent.objects.all().delete()
downloadRss('http://feed.rutracker.org/atom/f/2200.atom',2016)
updateMagnet()
