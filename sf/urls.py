from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()
from films.views import *

from films.comments import RateComment

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'films.views.index', name='index'),
    url(r'^film/(?P<uid>\w+)/$', 'films.views.film', name='film'),
    url(r'^genre/(?P<uid>\w+)/$', 'films.views.genre', name='genre'),
    url(r'^new/genre/(?P<uid>\w+)/$', 'films.views.newGenre', name='new_genre'),
    url(r'^actor/(?P<uid>\w+)/$', 'films.views.actor', name='actor'),
    url(r'^actor/(?P<uid>\w+)/(?P<page>\d+)/$', 'films.views.actor', name='actor'),
    url(r'^producer/(?P<uid>\w+)/$', 'films.views.producer', name='producer'),
    url(r'^producer/(?P<uid>\w+)/(?P<page>\d+)/$', 'films.views.producer', name='producer'),
    url(r'^country/(?P<uid>\w+)/$', 'films.views.country', name='country'),
    url(r'^country/(?P<uid>\w+)/(?P<page>\d+)/$', 'films.views.country', name='country'),
    url(r'^select/$', 'films.views.selectFilm', name='select_film'),
    
    url(r'^set/genre/(?P<gid>\d+)/(?P<fid>\d+)/$', 'films.views.setGenre', name='set_genre'),
    url(r'^add/rutracker/(?P<id>\d+)/$', 'films.views.addRutrackerLink', name='add_rutracker_link'),
    
    
    url(r'^register/$', 'films.views.registerUser', name='register'),
    url(r'^logout/$', 'films.views.logoutUser', name='logout'),
    url(r'^login/$', 'films.views.loginUser', name='login'),
    
    url(r'^bookmarks/$', 'films.views.bookmarks', name='bookmarks'),
    url(r'^likes/$', 'films.views.likes', name='likes'),
    url(r'^likes/(?P<id>\d+)/$', 'films.views.likes', name='likes'),
    url(r'^hidden/$', 'films.views.hidden', name='hidden'),
    
    url(r'^comment/(?P<uid>\w+)/$', 'films.views.comment', name='comment'),
    
    url(r'^api/search/$', 'films.api.search', name='api_search'),
    url(r'^api/info/(?P<id>\d+)/$', 'films.api.info', name='api_info'),
    
    url(r'^search/$', 'films.views.search', name='search'),

    url(r'^f/hide/(?P<id>\d+)/$', 'films.views.hideFilm', name='hide_film'),
    url(r'^f/like/(?P<id>\d+)/$', 'films.views.likeFilm', name='like_film'),
    url(r'^f/bookmark/(?P<id>\d+)/$', 'films.views.bookmarkFilm', name='bookmark_film'),
    url(r'^torrents/$', TorrentView.as_view(), name='torrent'),
    
    url(r'^comment/rate/(?P<pk>\d+)/(?P<rate>\d+)/$', RateComment.as_view(), name='rate_comment'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^ulogin/', include('django_ulogin.urls')),
)
