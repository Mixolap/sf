from films.models import *
from django.conf import settings
def vars(request):
    return {
        "genre_main": Genre.objects.filter(on_main=True),
        "genre_other":Genre.objects.filter(on_main=False),
    }