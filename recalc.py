#-*- encoding: utf-8 -*-
import os
from PIL import Image
import django,subprocess
from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sf.settings")
application = get_wsgi_application()

from django.db.models import Avg,Sum
from django.conf import settings
from films.models import *

ipath = "/opt/sf/static/imgs/"
opath = "/opt/sf/static/fi/"

ipath2 = "/opt/sf/static/name_img/"
opath2 = "/opt/sf/static/name_img_thumb/"

width = 200
height = 300



def recalcProducers():
    Producer.objects.all().update(rating=0)
    for d in Producer.objects.all():        
        if FilmProducer.objects.filter(producer=d).count()<10: continue
        r = FilmProducer.objects.filter(producer=d,film__kp_raiting__gte=7).exclude(film__kp_raiting=None).count()#.aggregate(Avg("film__kp_raiting"))["film__kp_raiting__avg"]
        d.rating = r
        d.save()
        print d.name,r

def recalcActors():
    Actor.objects.all().update(rating=0)
    for d in Actor.objects.all():        
        if FilmActor.objects.filter(actor=d).count()<15: continue
        r = FilmActor.objects.filter(actor=d,film__kp_raiting__gte=7).exclude(film__kp_raiting=None).count()#.aggregate(Sum("film__kp_raiting"))["film__kp_raiting__sum"]
        d.rating = r
        d.save()
        print d.name,r

def recalcCountry():
    Country.objects.all().update(rating=0)
    for d in Country.objects.all():        
        if FilmCountry.objects.filter(country=d).count()<20: continue
        r = FilmCountry.objects.filter(country=d,film__kp_raiting__gte=8).exclude(film__kp_raiting=None).count()#.aggregate(Sum("film__kp_raiting"))["film__kp_raiting__sum"]
        d.rating = r or 0
        d.save()
        print d.name,r

def removeNonExistingImgs(path):
    for d in os.listdir(path):
        id = int(d.split(".")[0])
        if Film.objects.filter(pk=id).count()==0:
            os.remove(path+d)

def removeFilmsWithoutImg():
    for d in Film.objects.all():
        if not os.path.exists(os.path.join(settings.BASE_DIR,"static/imgs/%d.jpg"%d.id)):
            print "for_remove",d.name
            d.delete()
            
def removeZeroRaitingFilms():
    for d in Film.objects.filter(kp_raiting__lte=2):
        print "removing",d.kp_raiting,d.name
        d.delete()
    
def updateFirstGenre():
    for d in Film.objects.all():
        is_korotko = FilmGenre.objects.filter(film=d,genre_id=70)
        if is_korotko.count()>0:
            is_korotko = is_korotko[0]
            is_korotko.is_first=1
            is_korotko.save()
            d.first_genre = is_korotko.genre
            d.save()
            continue
            
        if FilmGenre.objects.filter(film=d).order_by("id").count()==0: 
            print "no genre",d.name,d.id
            continue
        fg = FilmGenre.objects.filter(film=d).order_by("id")[0]        
        if fg.is_first==1: continue
        fg.is_first=1        
        fg.save()
        d.first_genre = fg.genre
        d.save()
        print fg.film.name,fg.genre.name

#updateFirstGenre()
#exit()

#removeNonExistingImgs(ipath)
#removeNonExistingImgs(opath)


#recalcProducers()
#recalcActors()
#recalcCountry()
removeZeroRaitingFilms()


for d in os.listdir(ipath):
    if os.path.getsize(ipath+d)==12902:
        os.remove(ipath+d)
        continue
    if os.path.exists(opath+d): continue
    img = Image.open(ipath+d)
    img.thumbnail((width,height))    
    img.save(opath+d)
    print d

for d in os.listdir(ipath2):
    if os.path.exists(opath2+d): continue
    print d
    img = Image.open(ipath2+d)
    img.thumbnail((100,100))    
    img.save(opath2+d)
    


removeFilmsWithoutImg()

updateFirstGenre()




