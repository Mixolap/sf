#-*- encoding: utf-8 -*-
from django.db import models
from django.db.models import Max
from django.contrib import admin
from django.contrib.auth.models import User
import random
## замена проблемных символов на латиницу
def ecran(s):
    try:
        s = s.decode('utf-8')
    except:
        pass
    s1 = [u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u"0",u"«",u"»",u"/",u"—",u'"',u"'",u']',u'[',u'?',u'+',u'•',u'#',u'–',u'а',u'б',u'в',u'г',u'д',u'е',u'ё',u'ж',u'з',u'и',u'й',u'к',u'л',u'м',u'н',u'о',u'п',u'р',u'с',u'т',u'у',u'ф',u'х',u'ц',u'ч',u'ш',u'щ',u'ъ',u'ы',u'ь',u'э',u'ю',u'я']
    s2 = [u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u"0",u" ",u" ",u" ",u" ",u" ",u" ",u' ',u' ',u' ',u' ',u'-',u'_','-','a','b','v','g','d','e','yo','j','z','i','i','k','l','m','n','o','p','r','s','t','u','f','x','ts','ch','sh','sh','','y','','e','y','ia','h','c','w','q']
    for i in xrange(len(s1)):        
        try:
            s = s.replace(s1[i],s2[i])
            s = s.replace(s1[i].upper(),s2[i].upper())      
        except:
            print s,s1[i],s2[i]
            raise x
    for i in xrange(len(s)):
        if s[i] in [".","-"]: continue        
        if s[i].lower() not in s2: 
            s=s.replace(s[i],"_")            
            continue
        if not (s[i].isalpha() or s[i].isnumeric()): s=s.replace(s[i],"_")
    return s


class Genre(models.Model):
    name    = models.CharField(max_length=255)
    title1  = models.CharField(max_length=255,null=True,blank=True)
    title2  = models.CharField(max_length=255,null=True,blank=True)
    uid     = models.CharField(max_length=255,null=True,blank=True)
    on_main = models.BooleanField(default=False)
    is_user = models.BooleanField(default=False)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Жанры"    
        ordering = ("name",)
    def save(self, *args, **kwargs):
        self.name = self.name[:50]
        if self.uid=='' or self.uid==None:
            tlink = ecran(self.name).replace(".","_").replace(" ","_").replace("-","").replace("__","_")
            tlink.strip("_")      
            if Genre.objects.filter(uid=tlink).count()>0: tlink=str(Genre.objects.all().order_by("-id")[0].id+2)+"_"+tlink
            self.uid = tlink.lower()
            if Genre.objects.filter(uid=self.uid).count()>0: self.uid = str(Genre.objects.aggregate(Max('id'))['id__max']+1)+'_'+self.uid
        self.uid = self.uid[:50]
        super(Genre, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None or self.uid=='': self.save()
        return '/genre/%s/' % self.uid
        
    def topFilms(self):
        fg = [f.film for f in FilmGenre.objects.filter(genre=self,film__on_main=True).order_by("-film__kp_raiting")[:10]]
        random.shuffle(fg)
        return fg[:8]
    
class Actor(models.Model):
    name   = models.CharField(max_length=255)
    rating = models.FloatField(default=0,null=True,blank=True)
    uid    = models.CharField(max_length=255,null=True,blank=True)
    kp_id  = models.IntegerField(null=True,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Актёры"
    def save(self, *args, **kwargs):
        self.name = self.name[:50]
        if self.uid=='' or self.uid==None:
            tlink = ecran(self.name).replace(".","_").replace(" ","_").replace("-","").replace("__","_")
            tlink.strip("_")      
            if Actor.objects.filter(uid=tlink).count()>0: tlink=str(Actor.objects.all().order_by("-id")[0].id+2)+"_"+tlink
            self.uid = tlink.lower()
            if Actor.objects.filter(uid=self.uid).count()>0: self.uid = str(Actor.objects.aggregate(Max('id'))['id__max']+1)+'_'+self.uid
        self.uid = self.uid[:50]
        super(Actor, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None or self.uid=='': self.save()
        return '/actor/%s/' % self.uid    
    
    def films(self):
        return [d.film for d in FilmActor.objects.filter(actor=self).order_by("-film__kp_raiting")]
        
class Producer(models.Model):
    name   = models.CharField(max_length=255)
    rating = models.FloatField(default=0,null=True,blank=True)
    uid    = models.CharField(max_length=255,null=True,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Режисёры"
    def save(self, *args, **kwargs):
        self.name = self.name[:50]
        if self.uid=='' or self.uid==None:
            tlink = ecran(self.name).replace(".","_").replace(" ","_").replace("-","").replace("__","_")
            tlink.strip("_")      
            if Producer.objects.filter(uid=tlink).count()>0: tlink=str(Producer.objects.all().order_by("-id")[0].id+2)+"_"+tlink
            self.uid = tlink.lower()
            if Producer.objects.filter(uid=self.uid).count()>0: self.uid = str(Producer.objects.aggregate(Max('id'))['id__max']+1)+'_'+self.uid
        self.uid = self.uid[:50]
        super(Producer, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None or self.uid=='': self.save()
        return '/producer/%s/' % self.uid     
        
    def films(self):
        return [d.film for d in FilmProducer.objects.filter(producer=self).order_by("-film__kp_raiting")]

class Country(models.Model):
    name   = models.CharField(max_length=255)
    rating = models.FloatField(default=0,null=True,blank=True)
    uid    = models.CharField(max_length=255,null=True,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Страны"
    def save(self, *args, **kwargs):
        self.name = self.name[:50]
        if self.uid=='' or self.uid==None:
            tlink = ecran(self.name).replace(".","_").replace(" ","_").replace("-","").replace("__","_")
            tlink.strip("_")      
            if Country.objects.filter(uid=tlink).count()>0: tlink=str(Country.objects.all().order_by("-id")[0].id+2)+"_"+tlink
            self.uid = tlink.lower()
            if Country.objects.filter(uid=self.uid).count()>0: self.uid = str(Country.objects.aggregate(Max('id'))['id__max']+1)+'_'+self.uid
        self.uid = self.uid[:50]
        super(Country, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None or self.uid=='': self.save()
        return '/country/%s/' % self.uid     
        
    def films(self):
        return [d.film for d in FilmCountry.objects.filter(country=self).order_by("-film__kp_raiting")[:10]]

class Film(models.Model):    
    name         = models.CharField(max_length=255)
    name_orig    = models.CharField(max_length=255,null=True,blank=True)
    year         = models.IntegerField()
    dt_out       = models.DateField(null=True,blank=True)
    description  = models.TextField(null=True,blank=True)
    kp_id        = models.IntegerField(null=True,blank=True)
    kp_raiting   = models.FloatField(default=0)
    imdb_raiting = models.FloatField(default=0)
    cnt_view     = models.IntegerField(default=0)
    cnt_like     = models.IntegerField(default=0)
    on_main      = models.BooleanField(default=True)
    uid          = models.CharField(max_length=255,null=True,blank=True)
    on_ff        = models.CharField(max_length=255,null=True,blank=True)
    rate         = models.FloatField(default=0)
    rate_count   = models.IntegerField(default=0)
    first_genre  = models.ForeignKey(Genre,null=True,blank=True)
    dt_add       = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    cnt_good_comment = models.IntegerField(default=0,verbose_name=u"Количество положительных комментариев")
    cnt_bad_comment  = models.IntegerField(default=0,verbose_name=u"Количество отрицательных комментариев")
    link         = models.CharField(max_length=255,null=True,blank=True,verbose_name=u"Ссылка на rutracker")
    dt_link      = models.DateTimeField(null=True,blank=True)
    is_downloaded = models.BooleanField(default=False)
    magnet       = models.CharField(max_length=1024,null=True,blank=True)
    
    def save(self, *args, **kwargs):
        self.name = self.name[:50]
        if self.uid=='' or self.uid==None:
            tlink = ecran(self.name).replace(".","_").replace(" ","_").replace("-","").replace("__","_")
            tlink.strip("_")      
            if Film.objects.filter(uid=tlink).count()>0: tlink=str(Film.objects.all().order_by("-id")[0].id+2)+"_"+tlink
            self.uid = tlink.lower()
            if Film.objects.filter(uid=self.uid).count()>0: 
                self.uid = self.uid+"_"+str(self.year)
        self.uid = self.uid[:50]
        #print "uid=",self.uid
        super(Film, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None or self.uid=='': self.save()
        return '/film/%s/' % self.uid
        
    def preview1(self):
        return "/static/fi/%d.jpg"%self.id
        
    def similar(self):
        sfs = []
        sfs.extend([f.film1 for f in SimilarFilm.objects.filter(film2=self)])
        sfs.extend([f.film2 for f in SimilarFilm.objects.filter(film1=self).exclude(film2__in=sfs)])
        sfs.sort(lambda x,y: cmp(y.kp_raiting,x.kp_raiting))
        return sfs        
        
        
    def actors(self):
        return [d.actor for d in FilmActor.objects.filter(film=self).order_by("id")]
    
    def actors_str(self):
        return ", ".join([d.name for d in self.actors()])
    
    def producers(self):
        return [d.producer for d in FilmProducer.objects.filter(film=self).order_by("id")]
    
    def producers_str(self):
        return ", ".join([d.name for d in self.producers()])
        
    def genre(self):
        return [d.genre for d in FilmGenre.objects.filter(film=self).order_by("id")]

    def genre_str(self):
        return ", ".join([d.name for d in self.genre()])

    def country(self):
        return [d.country for d in FilmCountry.objects.filter(film=self).order_by("id")]
        
    def country_str(self):
        return ", ".join([d.name for d in self.country()])
    
    def is72(self):
        return FilmGenre.objects.filter(film=self,genre_id=72).count()>0
    
    def good_proc(self):
        total = self.cnt_good_comment + self.cnt_bad_comment
        if total==0: return 0
        return (self.cnt_good_comment*100)/total
        
    def bad_proc(self):
        return 100 - self.good_proc()
        
    def genre_text(self):
        data = []
        for d in self.genre():
            data.append(d.name)
        return ", ".join(data)
        
    def actors_text(self):
        data = []
        for d in self.actors():
            data.append(d.name)
        return ", ".join(data)


class FilmGenre(models.Model):
    film     = models.ForeignKey(Film)
    genre    = models.ForeignKey(Genre)
    is_first = models.IntegerField(default=0)
    
class FilmActor(models.Model):
    film  = models.ForeignKey(Film)
    actor = models.ForeignKey(Actor)
    
class FilmProducer(models.Model):
    film  = models.ForeignKey(Film)    
    producer = models.ForeignKey(Producer)

class FilmCountry(models.Model):
    film  = models.ForeignKey(Film)    
    country = models.ForeignKey(Country)
    

class SimilarFilm(models.Model):
    film1 = models.ForeignKey(Film,related_name=u"film1")
    film2 = models.ForeignKey(Film,related_name=u"film2")

class LikeFilm(models.Model):
    user = models.ForeignKey(User)
    film = models.ForeignKey(Film)
    class Meta:
        ordering = ("-id",)
class HideFilm(models.Model):
    user = models.ForeignKey(User)
    film = models.ForeignKey(Film)
    class Meta:
        ordering = ("-id",)
        
class BookmarkFilm(models.Model):
    user = models.ForeignKey(User)
    film = models.ForeignKey(Film)
    class Meta:
        ordering = ("-id",)
class UserAdvice(models.Model):
    user  = models.ForeignKey(User,null=True,blank=True)
    genre = models.ForeignKey(Genre,null=True,blank=True)
    film  = models.ForeignKey(Film)
    kp_raiting = models.FloatField(null=True,blank=True)
    class Meta:
        ordering = ("-kp_raiting",)

class Comment(models.Model):
    user   = models.ForeignKey(User,null=True,blank=True)
    film   = models.ForeignKey(Film)
    rate   = models.IntegerField(default=0)
    text   = models.TextField(null=True,blank=True)
    dt     = models.DateTimeField(auto_now_add=True)
    xrate  = models.IntegerField(default=0,choices=[[0,'не определен'],[1,'отрицательный'],[2,'положительный'],]) # оценка комментария человеком (хороший/нейтральный/отрицательный)
    nsrate = models.IntegerField(default=0,choices=[[0,'не определен'],[1,'отрицательный'],[2,'положительный']]) # оценка комментария НС (хороший/нейтральный/отрицательный)
    ctext  = models.TextField(null=True,blank=True,verbose_name=u"Закодированный текст")
    is_rm  = models.BooleanField(default=False,verbose_name=u"Комментарий удалён")
    dt_rm  = models.DateTimeField(auto_now=True,verbose_name=u"Дата удаления комментария")
    def __unicode__(self):
        return "%d - %d - %s"%(self.xrate,self.nsrate,self.text)
        
    class Meta:
        verbose_name_plural = u"Комментарии"
        ordering = ("-nsrate",)


class Word(models.Model):
    word = models.CharField(max_length=255)

class Torrent(models.Model):
    name = models.TextField()
    filmname = models.TextField(null=True,blank=True,verbose_name=u"Расчётное наименование фильма")
    link = models.TextField()
    dt   = models.DateTimeField(auto_now_add=True)
    film = models.ForeignKey(Film,null=True,blank=True)
    is_removed = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.name
        
    def procName(self):
        nm = self.name.split("/")[0]
        nm = nm.replace(u"[Обновлено]","")
        return nm.strip()
    
    
admin.site.register(Genre)    
admin.site.register(Comment)    
admin.site.register(Word)    
admin.site.register(Torrent)    
