#-*- encoding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.db.models import Avg
from django.contrib.auth.models import User
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from models import *
import datetime
import json

def search(request):
    text = request.GET.get("text")
    data = []
    if text==None or len(text)<3: 
        return HttpResponse(json.dumps(data))
        
    xdata  = Film.objects.filter(name__icontains=text) | Film.objects.filter(name_orig__icontains=text)
    
    for d in xdata:
        data.append({
            "id":d.id,
            "runame":d.name,
            "enname":d.name_orig,
            "year":d.year,
        })
    
    return HttpResponse(json.dumps(data))
    
    
def info(request,id):
    fm = Film.objects.get(pk=id)
    data = {
        'id':fm.id,
        'title':fm.name,
        'country':fm.country_str(),
        'genre':fm.genre_str(),
        'y':fm.year,
        'producer':fm.producers_str(),
        'actors':fm.actors_str(),
        'summary':fm.description,
        'raiting':fm.kp_raiting,
        'kp_id':fm.kp_id,
        'premier_rus':fm.dt_out.strftime("%d.%m.%Y %H:%M"),
    }
    return HttpResponse(json.dumps(data))