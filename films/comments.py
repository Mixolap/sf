#-*- encoding: utf-8 -*-
from django.shortcuts import render
from django.db.models import Avg
from django.contrib.auth.models import User
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from models import *
import datetime



class RateComment(DetailView):
    model = Comment
    
    def get(self,request,*args,**kwargs):
        object = self.get_object()
        
        if request.GET.get("rm","")=="1":
            object.is_rm=True
            object.save()
            return HttpResponseRedirect(object.film.get_absolute_url())
        object.xrate  = kwargs['rate']
        object.nsrate = kwargs['rate']
        object.save()
        return HttpResponseRedirect(object.film.get_absolute_url())