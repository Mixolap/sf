# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0013_torrent_magnet'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='torrent',
            name='magnet',
        ),
        migrations.AddField(
            model_name='film',
            name='magnet',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
