# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0009_torrent_is_removed'),
    ]

    operations = [
        migrations.AddField(
            model_name='film',
            name='dt_link',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='film',
            name='link',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 rutracker', blank=True),
        ),
    ]
