# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0014_auto_20160919_1942'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='is_rm',
            field=models.BooleanField(default=False, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439 \u0443\u0434\u0430\u043b\u0451\u043d'),
        ),
    ]
