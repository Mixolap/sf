# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0011_torrent_filmname'),
    ]

    operations = [
        migrations.AddField(
            model_name='film',
            name='is_downloaded',
            field=models.BooleanField(default=False),
        ),
    ]
