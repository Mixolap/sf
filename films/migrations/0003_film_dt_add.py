# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0002_film_first_genre'),
    ]

    operations = [
        migrations.AddField(
            model_name='film',
            name='dt_add',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
