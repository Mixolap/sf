# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0005_auto_20160128_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='film',
            name='link',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
