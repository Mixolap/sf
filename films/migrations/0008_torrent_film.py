# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0007_torrent'),
    ]

    operations = [
        migrations.AddField(
            model_name='torrent',
            name='film',
            field=models.ForeignKey(blank=True, to='films.Film', null=True),
        ),
    ]
