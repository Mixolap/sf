# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0003_film_dt_add'),
    ]

    operations = [
        migrations.AddField(
            model_name='genre',
            name='is_user',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='genre',
            name='on_main',
            field=models.BooleanField(default=False),
        ),
    ]
