# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0008_torrent_film'),
    ]

    operations = [
        migrations.AddField(
            model_name='torrent',
            name='is_removed',
            field=models.BooleanField(default=False),
        ),
    ]
