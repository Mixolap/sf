# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0012_film_is_downloaded'),
    ]

    operations = [
        migrations.AddField(
            model_name='torrent',
            name='magnet',
            field=models.CharField(max_length=1024, null=True, blank=True),
        ),
    ]
