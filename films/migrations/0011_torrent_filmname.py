# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0010_auto_20160919_1826'),
    ]

    operations = [
        migrations.AddField(
            model_name='torrent',
            name='filmname',
            field=models.TextField(null=True, verbose_name='\u0420\u0430\u0441\u0447\u0451\u0442\u043d\u043e\u0435 \u043d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u0444\u0438\u043b\u044c\u043c\u0430', blank=True),
        ),
    ]
