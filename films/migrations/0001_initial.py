# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Actor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('rating', models.FloatField(default=0, null=True, blank=True)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
                ('kp_id', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': '\u0410\u043a\u0442\u0451\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='BookmarkFilm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rate', models.IntegerField(default=0)),
                ('text', models.TextField(null=True, blank=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('rating', models.FloatField(default=0, null=True, blank=True)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u044b',
            },
        ),
        migrations.CreateModel(
            name='Film',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('name_orig', models.CharField(max_length=255, null=True, blank=True)),
                ('year', models.IntegerField()),
                ('dt_out', models.DateField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('kp_id', models.IntegerField(null=True, blank=True)),
                ('kp_raiting', models.FloatField(default=0)),
                ('imdb_raiting', models.FloatField(default=0)),
                ('cnt_view', models.IntegerField(default=0)),
                ('cnt_like', models.IntegerField(default=0)),
                ('on_main', models.BooleanField(default=True)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
                ('on_ff', models.CharField(max_length=255, null=True, blank=True)),
                ('rate', models.FloatField(default=0)),
                ('rate_count', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='FilmActor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actor', models.ForeignKey(to='films.Actor')),
                ('film', models.ForeignKey(to='films.Film')),
            ],
        ),
        migrations.CreateModel(
            name='FilmCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.ForeignKey(to='films.Country')),
                ('film', models.ForeignKey(to='films.Film')),
            ],
        ),
        migrations.CreateModel(
            name='FilmGenre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_first', models.IntegerField(default=0)),
                ('film', models.ForeignKey(to='films.Film')),
            ],
        ),
        migrations.CreateModel(
            name='FilmProducer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('film', models.ForeignKey(to='films.Film')),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('title1', models.CharField(max_length=255, null=True, blank=True)),
                ('title2', models.CharField(max_length=255, null=True, blank=True)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
                ('on_main', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name_plural': '\u0416\u0430\u043d\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='HideFilm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('film', models.ForeignKey(to='films.Film')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='LikeFilm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('film', models.ForeignKey(to='films.Film')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='Producer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('rating', models.FloatField(default=0, null=True, blank=True)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': '\u0420\u0435\u0436\u0438\u0441\u0451\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='SimilarFilm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('film1', models.ForeignKey(related_name='film1', to='films.Film')),
                ('film2', models.ForeignKey(related_name='film2', to='films.Film')),
            ],
        ),
        migrations.CreateModel(
            name='UserAdvice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kp_raiting', models.FloatField(null=True, blank=True)),
                ('film', models.ForeignKey(to='films.Film')),
                ('genre', models.ForeignKey(blank=True, to='films.Genre', null=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-kp_raiting',),
            },
        ),
        migrations.AddField(
            model_name='filmproducer',
            name='producer',
            field=models.ForeignKey(to='films.Producer'),
        ),
        migrations.AddField(
            model_name='filmgenre',
            name='genre',
            field=models.ForeignKey(to='films.Genre'),
        ),
        migrations.AddField(
            model_name='comment',
            name='film',
            field=models.ForeignKey(to='films.Film'),
        ),
        migrations.AddField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bookmarkfilm',
            name='film',
            field=models.ForeignKey(to='films.Film'),
        ),
        migrations.AddField(
            model_name='bookmarkfilm',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
