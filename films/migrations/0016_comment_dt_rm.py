# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0015_comment_is_rm'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='dt_rm',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 2, 18, 19, 42, 639967, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0443\u0434\u0430\u043b\u0435\u043d\u0438\u044f \u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u044f', auto_now=True),
            preserve_default=False,
        ),
    ]
