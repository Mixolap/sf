#-*- encoding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.db.models import Avg
from django.contrib.auth.models import User
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from models import *
from sf.ff import ffId
import datetime

def catch_ulogin_signal(*args, **kwargs):
    """
    Обновляет модель пользователя: исправляет username, имя и фамилию на
    полученные от провайдера.

    В реальной жизни следует иметь в виду, что username должен быть уникальным,
    а в социальной сети может быть много "тёзок" и, как следствие,
    возможно нарушение уникальности.

    """
    user=kwargs['user']
    json=kwargs['ulogin_data']

    if kwargs['registered']:
        user.first_name=json['first_name']
        user.last_name=json['last_name']        
        user.save()
        #u = UserProfile(user=user,ip="ulogin",name=json['first_name']+" "+json['last_name'])
        #if 'photo' in json.keys(): u.net_avatar = json['photo']
        #u.save()

try:
    from django_ulogin.models import ULoginUser
    from django_ulogin.signals import assign
    assign.connect(receiver = catch_ulogin_signal,
                   sender   = ULoginUser,
                   dispatch_uid = 'customize.models')
except:
    print "NO ULOGIN"



def registerUser(request,template_name="register.html"):
    if request.method=="POST":
        username = request.POST.get("username").strip()
        email    = request.POST.get("password","no@no.ru")
        password = request.POST.get("password1")        
        ref      = request.POST.get("ref","/")
        if ref=="" or "/login/" in ref or "/register" in ref or ref=="None": ref="/"
        if User.objects.filter(username__iexact=username).count()>0:
            msg=u"Имя пользователя "+username+u" уже занято. Выберите другое имя."
            return render(request,template_name,locals())
        if password=="": 
            msg=u"Введите пароль"
            return render(request,template_name,locals())            
        luser = User.objects.create_user(username=username,email=email,password=password)
        luser.save()
        if luser==None: 
            msg=u"Имя пользователя занято"
            return render(request,template_name,locals())
        luser = authenticate(username=username,password=password)
        if luser==None: 
            msg=u"Неверное имя пользователя или пароль"
            return render(request,template_name,locals())        
        login(request,luser)
        if ref==None: ref="/"        
        return HttpResponseRedirect(ref)
    else:
        if request.user.is_anonymous(): 
            ref = request.GET.get("next")
            if ref==None:
                ref = request.META.get("HTTP_REFERER")
            return render(request,template_name,locals())
        logout(request)
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))     
    
def loginUser(request,template_name="login.html"):
    redirect_to = request.REQUEST.get('next', '/')

    if request.method=="POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        ref      = request.POST.get("ref","/administrator/")
        if ref=="" or "/login" in ref: ref="/"
        luser = authenticate(username=username, password=password)
        if luser==None: 
            msg=u"Неверное имя пользователя или пароль"
            return render(request,template_name,locals())
        login(request,luser)
        
        return HttpResponseRedirect(ref)
        #return HttpResponseRedirect("/")
    else:
        if request.user.is_anonymous(): 
            ref = request.GET.get("next")
            if ref==None:
                ref = request.META.get("HTTP_REFERER")
            return render(request,template_name,locals())
        #logout(request)
        return HttpResponseRedirect(request.META.get("HTTP_REFERER")) 
        #return HttpResponseRedirect("/")


@login_required
def logoutUser(request):
    logout(request)
    next = request.GET.get("next","/")
    return HttpResponseRedirect(next)

# удалить сериалы, короткометражки, тв
def deleteFilms():
    
    Film.objects.filter(name__icontains=u"сериал").delete()
    Film.objects.filter(name__icontains=u"(ТВ)").delete()
    Film.objects.filter(name__icontains=u"(видео)").delete()
    Film.objects.filter(year__lte=1980).delete()    
    
    #g = Genre.objects.filter(name__iexact=u"короткометражка")
    #for d in FilmGenre.objects.filter(genre=g):
    #    d.film.delete()
    #g.delete()

    #g = Genre.objects.filter(name__iexact=u"мультфильм")
    #for d in FilmGenre.objects.filter(genre=g):
    #    d.film.delete()
    #g.delete()
    
    g = Genre.objects.filter(name__iexact=u"документальный")
    for d in FilmGenre.objects.filter(genre=g):
        d.film.delete()
    g.delete()    

    g = Genre.objects.filter(name__iexact=u"фильм-нуар")
    for d in FilmGenre.objects.filter(genre=g):
        d.film.delete()
    g.delete()   
    
    for g in Genre.objects.all():
        if FilmGenre.objects.filter(genre=g).count()==0: g.delete()
    
def index(request,template_name="index.html"):
    genre = Genre.objects.filter(on_main=True)
    producers = Producer.objects.filter(rating__gt=0).order_by("-rating")[:20]
    actors    = Actor.objects.filter(rating__gt=0).order_by("-rating")[:20]
    countries = Country.objects.filter(rating__gt=0).order_by("-rating")[:20]
    #deleteFilms()
    
    #best_films = recomendFilms(request,Genre.objects.get(pk=6),6)
    #genre_films = []
    #for d in genre:
    #    genre_films.append({
    #        "genre":d,
    #        "films":recomendFilms(request,Genre.objects.get(pk=6),6),
    #    })
    last_films = Film.objects.filter(dt_out__lte=datetime.datetime.now(),kp_raiting__gte=5).order_by("-dt_out")[:60]
    on_main = True
    return render(request,template_name,locals())
    
def film(request,uid,template_name="film.html"):
    film = Film.objects.get(uid=uid)
    may_comment = request.user.is_authenticated() and Comment.objects.filter(film=film,user=request.user).count()==0 
    comments = Comment.objects.filter(film=film,is_rm=False).exclude(text=None).order_by("id")
    if request.user.is_anonymous():
        is_like     = request.session.get("like_films")==None or film.id not in request.session.get("like_films")
        is_bookmark = request.session.get("bookmark_films")==None or film.id not in request.session.get("bookmark_films")
    else:
        is_like     = LikeFilm.objects.filter(film=film,user=request.user).count()==0
        is_bookmark = BookmarkFilm.objects.filter(film=film,user=request.user).count()==0
    if film.on_ff==None:
        furl = ffId(film.name,film.year)
        if furl!=0 and furl!=None:
            film.on_ff = "http://ftpface.ru/%d/"%furl
            film.save()
    return render(request,template_name,locals())
    
def genre(request,uid,template_name="genre.html"):
    genre = Genre.objects.get(uid=uid)
    user  = request.user
    cnt   = int(request.GET.get("cnt",8))
    
    data = recomendFilms(request,genre)
    
    if request.is_ajax():
        template_name = "films_list.htm"
    
    return render(request,template_name,locals())


def newGenre(request,uid,template_name="new_genre.html"):
    genre = Genre.objects.get(uid=uid)
    user  = request.user
    count   = int(request.GET.get("cnt",100))
    
    no_films = []
    
    if request.user.is_anonymous():
        no_films = hideFilmsFromSession(request)
    else:
        no_films = hideFilmsFromBase(request)
    
    data = [d.film_id for d in FilmGenre.objects.filter(genre=genre).exclude(film_id__in=no_films).exclude(film__dt_out=None).order_by("-film__dt_out")[:count]]
    data = Film.objects.filter(pk__in=data).order_by("-dt_out")
        
    """
    if not request.user.is_anonymous():
        ua = UserAdvice.objects.filter(user=user,genre=genre)
        if ua.count()<cnt:
            print "update cache"
            ua.delete()
            data = recomendFilms(request,cnt,request.GET.get("id"),genre)
            for d in data:
                UserAdvice(user=user,genre=genre,film=d).save()
        elif request.GET.get("id")!=None or request.GET.get("like_id")!=None or request.GET.get("bookmark_id")!=None:
            print "clear cache"
            ua.delete()
            data = recomendFilms(request,8,request.GET.get("id"),genre)
            for d in data:
                UserAdvice(user=user,genre=genre,film=d).save()
            data = data[:cnt]
        else:
            print "from cache"
            data = [d.film for d in ua[:8]]
    else:
        data = recomendFilms(request,cnt,request.GET.get("id"),genre)
    """
    if request.is_ajax():
        template_name = "films_list.htm"
    return render(request,template_name,locals())


def selectFilm(request,template_name="select_film.html"):
    data = recomendFilms(request,8)
    return render(request,template_name,locals())

def fromSession(request,name,id=None):
    nf = request.session.get(name)
    if nf==None:
        request.session[name]=[]
        nf = []
    if id!=None: nf.append(int(id))
    nf = [int(d) for d in nf]        
    request.session[name]=nf
    return nf    

def hideFilmsFromSession(request):
    
    nf = fromSession(request,"no_films",request.GET.get("hide_id")) or []
    lf = fromSession(request,"like_films",request.GET.get("like_id")) or []
    bf = fromSession(request,"bookmark_films",request.GET.get("bookmark_id")) or []
    return nf+bf+lf

def hideFilmsFromBase(request):
    user = request.user
    
    hid = request.GET.get("hide_id")
    lid = request.GET.get("like_id")
    bid = request.GET.get("bookmark_id")
    if hid!=None: 
        hf = HideFilm.objects.filter(user=request.user,film_id=hid)
        if hf.count()>0: hf.delete()
        else:
            HideFilm(user=request.user,film_id=hid).save()
            
    if lid!=None: 
        hf = LikeFilm.objects.filter(user=request.user,film_id=lid)
        if hf.count()>0: hf.delete()
        else:
            LikeFilm(user=request.user,film_id=lid).save()
            
    if bid!=None: 
        hf = BookmarkFilm.objects.filter(user=request.user,film_id=bid)
        if hf.count()>0: hf.delete()
        else:
            BookmarkFilm(user=request.user,film_id=bid).save()            
            
    hf = [d.film_id for d in HideFilm.objects.filter(user=user)]
    bm = [d.film_id for d in BookmarkFilm.objects.filter(user=user)]
    lf = [d.film_id for d in LikeFilm.objects.filter(user=user)]
    return bm+hf+lf

def filmInData(f,data):
    for d in data:
        if d.id==f.id: return True
    return False


def similar(d):
    sfs = []
    sfs.extend([f.film1_id for f in SimilarFilm.objects.filter(film2_id=d)])
    sfs.extend([f.film2_id for f in SimilarFilm.objects.filter(film1_id=d).exclude(film2__in=sfs)])
    return sfs        
    
def getLikeFilms(request,genre=None):
    user = request.user
    lf = []
    if user.is_anonymous(): 
        lf = [int(d) for d in request.session["like_films"]]
    else:
        return [d.film_id for d in LikeFilm.objects.filter(user=user)]
        
    if len(lf)==0: return []
    data = []
    for d in lf:
        for f in similar(d):
            if genre!=None and FilmGenre.objects.filter(film=f,genre=genre,is_first=1).count()==0: continue
            if user.is_anonymous():
                if f in request.session["like_films"]:     continue
                if f in request.session["no_films"]:       continue
                if f in request.session["bookmark_films"]: continue
            else:
                if HideFilm.objects.filter(user=user,film_id=f).count()>0: continue
                if BookmarkFilm.objects.filter(user=user,film_id=f).count()>0: continue
                if LikeFilm.objects.filter(user=user,film_id=f).count()>0: continue
            if f in data: continue
            data.append(f)
    return data
    #data = Film.objects.filter(pk__in=data).order_by("-kp_raiting")
    
    #return [d.id for d in data]
    
def getHiddenFilms(request):
    user = request.user
    if user.is_anonymous(): 
        return [int(d) for d in request.session["no_films"]]
    else:
        return [d.film_id for d in HideFilm.objects.filter(user=user)]

def getBookmarkFilms(request):
    user = request.user
    if user.is_anonymous(): 
        return [int(d) for d in request.session["bookmark_films"]]
    else:
        return [d.film_id for d in BookmarkFilm.objects.filter(user=user)]    

def updateUserAdvice(request,genre):
    print "updating cache"
    user = request.user
    if request.user.is_anonymous():
        user = None
    UserAdvice.objects.filter(user=user,genre=genre).delete()
    rf = recomendFilms(request,10,None,genre,True)
    for d in rf:
        UserAdvice(user=user,genre=genre,film=d,kp_raiting=d.kp_raiting).save()
    return rf
    



def recomendFilms(request,genre,cnt=100):
    if request.user.is_anonymous():
        no_films = hideFilmsFromSession(request)
    else:
        no_films = hideFilmsFromBase(request)
    
    data = []
    
    #cnt = request.GET.get("cnt",100)
    nw = datetime.datetime.now()
    if genre!=None: 
        if not genre.is_user:
            films = Film.objects.filter(first_genre=genre,year__gte=nw.year-15).exclude(id__in=no_films).order_by("-kp_raiting","-year","-id")[:cnt]
        else:
            films = [f.film for f in FilmGenre.objects.filter(genre=genre).exclude(film_id__in=no_films)[:cnt]]

        return films
        
    return Film.objects.filter(pk__in=data).order_by("-kp_raiting")

def hideFilm(request,id,template_name="films_list.htm"):
    if not request.user.is_anonymous():
        HideFilm.objects.get_or_create(user=request.user,film=Film.objects.get(pk=id))
        UserAdvice.objects.filter(user=request.user).delete()
        
    data = recomendFilms(request,6,id)
    return render(request,template_name,locals())
    
def likeFilm(request,id,template_name="films_list.htm"):
    if not request.user.is_anonymous():
        print "liking"
        m,cr = LikeFilm.objects.get_or_create(user=request.user,film=Film.objects.get(pk=id))
        print "cr",cr
        if cr==False: m.delete()
        UserAdvice.objects.filter(user=request.user).delete()
        if request.GET.get("noreload")=="1": return HttpResponse('ok')
    data = recomendFilms(request,6)
    return render(request,template_name,locals())
    
def bookmarkFilm(request,id,template_name="films_list.htm"):
    if not request.user.is_anonymous():
        bm,cr = BookmarkFilm.objects.get_or_create(user=request.user,film=Film.objects.get(pk=id))
        if cr==False: bm.delete()
        UserAdvice.objects.filter(user=request.user).delete()
        if request.GET.get("noreload")=="1": return HttpResponse('ok')
        
    data = recomendFilms(request,6)
    return render(request,template_name,locals())    
    
def actor(request,uid,page=1,template_name="actor.html"):
    page = int(page)
    per_page = 10
    data = Actor.objects.get(uid=uid)    
    films = [d.film for d in FilmActor.objects.filter(actor=data).order_by("-film__kp_raiting","-id")[(page-1)*per_page:page*per_page]]
    total_cnt = FilmActor.objects.filter(actor=data).count()
    if total_cnt>page*per_page:
        next_page = page+1
        next_cnt  = total_cnt - page*per_page
        if next_cnt>10: next_cnt = 10
         
    return render(request,template_name,locals())    
    
def producer(request,uid,page=1,template_name="producer.html"):
    page = int(page)
    per_page = 10
    data = Producer.objects.get(uid=uid)    
    films = [d.film for d in FilmProducer.objects.filter(producer=data).order_by("-film__year","-id")[(page-1)*per_page:page*per_page]]
    total_cnt = FilmProducer.objects.filter(producer=data).count()
    if total_cnt>page*per_page:
        next_page = page+1
        next_cnt  = total_cnt - page*per_page
        if next_cnt>10: next_cnt = 10
    return render(request,template_name,locals())    
    
def country(request,uid,page=1,template_name="country.html"):
    page = int(page)
    per_page = 10
    data = Country.objects.get(uid=uid)    
    films = [d.film for d in FilmCountry.objects.filter(country=data).order_by("-film__kp_raiting","-id")[(page-1)*per_page:page*per_page]]
    total_cnt = FilmCountry.objects.filter(country=data).count()
    if total_cnt>page*per_page:
        next_page = page+1
        next_cnt  = total_cnt - page*per_page
        if next_cnt>10: next_cnt = 10
    return render(request,template_name,locals())    

def bookmarks(request,template_name="bookmarks.html"):
    if request.user.is_anonymous():
        films = [Film.objects.get(pk=d) for d in fromSession(request,"bookmark_films")]
    else:
        films = [d.film for d in BookmarkFilm.objects.filter(user=request.user)]
    return render(request,template_name,locals())    

def likes(request,id=0,template_name="likes.html"):
    if id!=0:
        films = [d.film for d in LikeFilm.objects.filter(user_id=id)]
        touser = User.objects.get(pk=id)
    else:
        if request.user.is_anonymous():
            films = [Film.objects.get(pk=d) for d in fromSession(request,"like_films")]
        else:
            films = [d.film for d in LikeFilm.objects.filter(user=request.user)]
    return render(request,template_name,locals())    
    
def hidden(request,template_name="hidden.html"):
    if request.user.is_anonymous():
        films = [Film.objects.get(pk=d) for d in fromSession(request,"no_films")]
    else:
        films = [d.film for d in HideFilm.objects.filter(user=request.user)]
    return render(request,template_name,locals())    

@login_required
def comment(request,uid):
    film = Film.objects.get(uid=uid)
    
    rate = int(request.GET.get("rate",0))
    if rate!=0:
        c,cr = Comment.objects.get_or_create(user=request.user,film=film)
        c.rate = rate
        c.save()
        
        film.rate = Comment.objects.filter(film=film,rate__gt=0).aggregate(Avg("rate"))["rate__avg"] or 0
        film.rate_count = Comment.objects.filter(film=film,rate__gt=0).count()
        film.save()
        return HttpResponse('ok')
        
    if request.method=="POST":        
        text = request.POST.get("text")
        c,cr = Comment.objects.get_or_create(user=request.user,film=film)
        if text!=None and text.strip()!="": c.text = text        
        c.save()
        
    return HttpResponseRedirect(film.get_absolute_url())
    
def search(request,template_name="search.html"):
    q = request.GET.get("q")
    data = Film.objects.filter(name__icontains=q).order_by("-kp_raiting","-year")[:100]
    return render(request,template_name,locals())    
    
def setGenre(request,fid,gid):
    film = Film.objects.get(id=fid)
    gr   = Genre.objects.get(pk=gid)
    
    fg = FilmGenre.objects.filter(film=film,genre=gr)
    if fg.count()>0: 
        fg.delete()
    else:
        FilmGenre(film=film,genre=gr).save()
    
    return HttpResponseRedirect(film.get_absolute_url())
    
def addRutrackerLink(request,id):
    film = Film.objects.get(id=id)
    film.link = request.POST.get("link")
    film.save()
    return HttpResponseRedirect(film.get_absolute_url())

class TorrentView(ListView):
    queryset = Torrent.objects.filter(film=None,is_removed=False)
    template_name = 'films/torrent_list.html'
    def get(self,request,*args,**kwargs):
        object_list = self.get_queryset()
        
        for d in object_list:
            d.films = Film.objects.filter(name__iexact=d.procName(),year=2016)
        last_added = Film.objects.filter(dt_link__isnull=False).order_by("-dt_link")[:30]
        return render(request,self.template_name,locals())
    