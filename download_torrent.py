#-*- encoding: utf-8 -*-
import os
import urllib
import requests
#from django.utils import timezone
from bs4 import BeautifulSoup

from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sf.settings")
application = get_wsgi_application()
from films.models import Film

import subprocess

def fileList(path,skip=''):
    files = []
    for d in os.listdir(path):        
        fname = os.path.join(path,d)
        if skip!='' and skip in fname: continue
        if os.path.isdir(fname):
            files.extend(fileList(fname))
        if fname[-3:].lower() in ['avi','mkv','mp4']:
            files.append(fname)
    return files
        
        
        
        
        
#path = '/home/ftp/upload/sf/'
path = '/mnt/disk1000/films/torrs/'
for d in Film.objects.filter(link__isnull=False,is_downloaded=False,magnet__isnull=False).order_by("-id"):
    #print d.kp_id,d.magnet
    magnet = urllib.unquote(d.magnet) 
    print d.id,d.kp_id,d.name,d.year,magnet
    flist1 = fileList(path)    
    cline = "transmission-cli -w '%s' -f /opt/sf/killtrans.sh '%s'"%(path,magnet)
    print cline
    prog = subprocess.Popen(cline, shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = prog.communicate()
    if int(prog.returncode)==1:
        print out
        print err
    flist2 = fileList(path)
    
    filepath = ''
    for fpath in flist2:
        if fpath not in flist1:
            filepath = fpath
            break
            
    if filepath=='': 
        print "SKIPPING"
        continue
    print "filepath=",filepath
    requests.get('http://ftpface.ru/api/check/',{'kp':d.kp_id,'name':d.name,'path':filepath})
    d.is_downloaded=True
    d.save()
    #break



#print fileList(path,'Saw')    
#print fileList(path)    
    
    
    
